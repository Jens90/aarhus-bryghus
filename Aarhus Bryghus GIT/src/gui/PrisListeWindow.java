package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PrisListe;
import service.Service;

public class PrisListeWindow extends Stage {
	private Service service;
	private Button btnOpret = new Button("Opret prisliste");
	private Button btnFortryd = new Button("Tilbage");
	private Button btnUpdate = new Button("Update");
	private Label lblError = new Label();
	private TextField txfNavn;
	private ListView<PrisListe> lvwPrislister = new ListView<>();

	public PrisListeWindow() {
		service = Service.getService();

		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		this.setTitle("Opret prisliste!");

		GridPane pane = new GridPane();
		Scene scene = new Scene(pane);
		this.initContent(pane);
		this.setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		pane.add(new Label("Opret prisliste: "), 0, 1);

		txfNavn = new TextField();
		pane.add(new Label("Prisliste navn: (*)"), 0, 2);
		pane.add(txfNavn, 1, 2);

		btnOpret.setMinWidth(100);
		btnOpret.setOnAction(event -> opretPrisListe());
		btnFortryd.setMinWidth(100);
		btnFortryd.setOnAction(event -> this.hide());
		btnUpdate.setMinWidth(100);
		btnUpdate.setOnAction(event -> opretUpdate());

		pane.add(lvwPrislister, 0, 3, 2, 1);
		lvwPrislister.setMaxHeight(100);
		lvwPrislister.getItems().addAll(service.getPrisListe());

		HBox hbox = new HBox(10);
		hbox.alignmentProperty().set(Pos.BOTTOM_CENTER);
		hbox.getChildren().add(btnOpret);
		hbox.getChildren().add(btnUpdate);
		hbox.getChildren().add(btnFortryd);

		pane.add(hbox, 0, 4, 2, 1);

		lblError.setTextFill(Color.RED);
		pane.add(lblError, 0, 5);
	}

	private void opretPrisListe() {
		String navn = txfNavn.getText();

		if (navn.isEmpty() == false) {
			service.opretPrisliste(navn);
			txfNavn.clear();
			lblError.setText("");

		} else {
			lblError.setText("Listen findes allerede!");
		}

		lvwPrislister.getItems().clear();
		lvwPrislister.getItems().addAll(service.getPrisListe());
	}

	private void opretUpdate() {

	}

}
