package gui;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class OpretPane extends GridPane {
	private Button btnOpretProdukt, btnOpretPrisListe;
	private GridPane opretPane = new GridPane();

	public OpretPane() {
		this.setHgap(30);
		this.setVgap(30);
		opretPane.setGridLinesVisible(false);

		btnOpretProdukt = new Button("Her");
		btnOpretPrisListe = new Button("Her");

		this.add(new Label("Oprettelse: "), 1, 1);
		this.add(new Label("Opret Produkt/Updatér: "), 1, 2);
		this.add(new Label("Opret Prisliste: "), 1, 3);

		this.add(btnOpretProdukt, 2, 2);
		this.add(btnOpretPrisListe, 2, 3);

		btnOpretProdukt.setOnAction(event -> actionOpretProdukt());
		btnOpretPrisListe.setOnAction(event -> actionOpretListe());
	}

	public void actionOpretProdukt() {
		ProduktWindow produkt = new ProduktWindow();
		produkt.updatePriser();
		produkt.showAndWait();
	}

	public void actionOpretListe() {
		PrisListeWindow prisliste = new PrisListeWindow();
		prisliste.showAndWait();
	}
}