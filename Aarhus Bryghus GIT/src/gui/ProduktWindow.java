package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.converter.IntegerStringConverter;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import service.Service;
import storage.Storage;

public class ProduktWindow extends Stage {
	private Button btnOpret = new Button("Opret");
	private Button btnFortryd = new Button("Tilbage");
	private Button btnFjern = new Button("Slet");
	private Label lblError = new Label();
	private TextField txfPris, txfBeskrivelse, txfNavn;
	private Button btnUpdate = new Button("Update");
	private Button btnTilføj = new Button("Tilføj til prisliste");
	private Button btnFjernListe = new Button("Fjern fra prisliste");
	private ComboBox<PrisListe> cbPris = new ComboBox<>();
	private ListView<Produkt> lvwProdukt;
	private ListView<PrisLinje> lvwPrisLinje;
	private Service service;

	public ProduktWindow() {
		service = Service.getService();

		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		this.setTitle("Opret produkt!");

		GridPane pane = new GridPane();
		Scene scene = new Scene(pane);
		this.initContent(pane);
		this.setScene(scene);
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		pane.add(new Label("Opret Produkt: (Vælg et produkt)"), 0, 1);

		txfNavn = new TextField();
		pane.add(new Label("Produkt navn: (*)"), 0, 2);
		pane.add(txfNavn, 1, 2);

		txfBeskrivelse = new TextField();
		pane.add(new Label("Bskrivelse: (*) "), 0, 3);
		pane.add(txfBeskrivelse, 1, 3);

		HBox hboxListe = new HBox(10);
		hboxListe.alignmentProperty().set(Pos.CENTER);
		hboxListe.getChildren().add(btnTilføj);
		hboxListe.getChildren().add(btnFjernListe);
		pane.add(hboxListe, 0, 9, 2, 1);

		pane.add(new Label("Prisliste: "), 0, 8);
		pane.add(new Label("Tilføj til prisliste: "), 0, 6);
		pane.add(cbPris, 1, 8);

		cbPris.getItems().addAll(service.getPrisListe());
		cbPris.getSelectionModel().selectFirst();
		cbPris.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updatePriser();
		});

		txfPris = new TextField();
		pane.add(new Label("Pris: "), 0, 7);
		pane.add(txfPris, 1, 7);

		lvwProdukt = new ListView<>();
		lvwPrisLinje = new ListView<>();
		pane.add(new Label("Produkter: "), 2, 1);
		pane.add(lvwProdukt, 2, 2, 2, 9);
		lvwProdukt.getItems().setAll(service.getProdukt());
		lvwProdukt.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updateDetails();
		});

		pane.add(new Label("Produkt prislister"), 4, 1);
		pane.add(lvwPrisLinje, 4, 2, 4, 9);

		btnOpret.setMinWidth(50);
		btnOpret.setOnAction(event -> opretProdukt());
		btnFortryd.setMinWidth(50);
		btnFortryd.setOnAction(event -> this.hide());
		btnUpdate.setMinWidth(50);
		btnUpdate.setOnAction(event -> opretUpdate());
		btnFjern.setMinWidth(50);
		btnFjern.setOnAction(event -> deleteProdukt());
		btnTilføj.setOnAction(event -> tilføjTilPrisliste());
		btnFjernListe.setOnAction(event -> fjernFraListe());

		HBox hbox = new HBox(10);
		hbox.alignmentProperty().set(Pos.BOTTOM_CENTER);
		hbox.getChildren().add(btnOpret);
		hbox.getChildren().add(btnUpdate);
		hbox.getChildren().add(btnFjern);
		hbox.getChildren().add(btnFortryd);

		pane.add(hbox, 0, 10, 2, 1);

		final TextFormatter<Integer> formatter = new TextFormatter<>(new IntegerStringConverter());
		txfPris.setTextFormatter(formatter);

		lblError.setTextFill(Color.RED);
		pane.add(lblError, 0, 11);
	}

	public void updatePriser() {
		lvwPrisLinje.getItems().clear();
		for (int i = 0; i < service.getPrisLinje().size(); i++) {
			if (service.getPrisLinje().get(i).getPrisListe() == cbPris.getSelectionModel().getSelectedItem()) {
				lvwPrisLinje.getItems().add(service.getPrisLinje().get(i));
			}
		}
	}

	private void deleteProdukt() {
		Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			Storage.removeProdukt(produkt);
			lvwProdukt.getItems().setAll(service.getProdukt());
			clearTxf();
		} else {
			lblError.setText("Vælg et produkt");
		}
	}

	private void opretUpdate() {
		Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			lblError.setText("Vælg et produkt");
		} else if (txfBeskrivelse.getText().isEmpty() == false && txfNavn.getText().isEmpty() == false) {
			produkt.setNavn(txfNavn.getText());
			produkt.setBeskrivelse(txfBeskrivelse.getText());
			lvwProdukt.getItems().setAll(service.getProdukt());

		} else {
			lblError.setText("Udfyld både navn og beskrivelse");
		}

	}

	private void updateDetails() {
		Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();

		if (produkt != null) {
			txfNavn.setText(produkt.getNavn());
			txfBeskrivelse.setText(produkt.getBeskrivelse());
		}

	}

	private void opretProdukt() {
		if (txfNavn.getText().isEmpty() == false && txfBeskrivelse.getText().isEmpty() == false) {
			service.opretProdukt(txfNavn.getText(), txfBeskrivelse.getText());
			lvwProdukt.getItems().setAll(service.getProdukt());
			clearTxf();
		} else {
			lblError.setText("Udfyld både navn og beskrivelse");
		}
	}

	private void tilføjTilPrisliste() {
		Produkt produkt = lvwProdukt.getSelectionModel().getSelectedItem();
		PrisListe prisListe = cbPris.getSelectionModel().getSelectedItem();
		if (produkt != null && prisListe != null && txfPris.getText().isEmpty() == false) {
			service.getPrisListe().get(cbPris.getSelectionModel().getSelectedIndex())
					.addPrisLinje(service.opretPrisLinje(Integer.parseInt(txfPris.getText()),
							lvwProdukt.getSelectionModel().getSelectedItem(),
							service.getPrisListe().get(cbPris.getSelectionModel().getSelectedIndex())));
			clearTxf();

		} else {
			lblError.setText("Vælg et produkt, pris og prisliste");
		}
		updatePriser();

	}

	private void fjernFraListe() {

		for (int i = 0; i < service.getPrisLinje().size(); i++) {

			if (service.getPrisLinje().get(i) == lvwPrisLinje.getSelectionModel().getSelectedItem()
					&& service.getPrisLinje().get(i).getPrisListe() == cbPris.getSelectionModel().getSelectedItem()) {
				Storage.removePrisLinje(service.getPrisLinje().get(i));
				System.out.println("Fjernet");
			}
		}
		updatePriser();

	}

	private void clearTxf() {
		txfBeskrivelse.clear();
		txfNavn.clear();
		txfPris.clear();
		lblError.setText("");
	}
}
