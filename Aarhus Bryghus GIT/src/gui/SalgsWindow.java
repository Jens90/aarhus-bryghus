package gui;

import java.time.LocalDateTime;
import java.util.ArrayList;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.converter.IntegerStringConverter;
import model.BetalingsMetoder;
import model.PrisLinje;
import model.PrisListe;
import model.SalgsLinje;
import service.Service;

public class SalgsWindow extends GridPane {
	private Service service;
	private Button btnkøb, btnTilføj, btnFjern;
	private TextField txfPris, txfAntal, txfTotalPris;
	private ListView<PrisLinje> lvwProdukt;
	private ListView<SalgsLinje> lvwKøbProdukter;
	private GridPane opretPane = new GridPane();
	private ComboBox<PrisListe> drpList = new ComboBox<PrisListe>();
	private ComboBox<BetalingsMetoder> betaling = new ComboBox<BetalingsMetoder>();
	private Label lblError = new Label();

	public SalgsWindow() {
		service = Service.getService();
		this.setHgap(20);
		this.setVgap(10);
		opretPane.setGridLinesVisible(true);

		betaling = new ComboBox<BetalingsMetoder>();
		betaling.getItems().setAll(BetalingsMetoder.values());

		btnTilføj = new Button("Tilføj");
		btnkøb = new Button("Køb");
		btnFjern = new Button("Fjern");
		lvwProdukt = new ListView<PrisLinje>();
		lvwKøbProdukter = new ListView<SalgsLinje>();
		txfPris = new TextField();
		txfPris.setEditable(false);
		txfAntal = new TextField();
		txfTotalPris = new TextField();
		txfTotalPris.setEditable(false);

		final TextFormatter<Integer> formatter = new TextFormatter<>(new IntegerStringConverter());
		txfAntal.setTextFormatter(formatter);

		this.add(betaling, 4, 3);
		this.add(new Label("Vælg Prisliste: "), 0, 0);
		this.add(lvwProdukt, 1, 1, 2, 10);
		this.add(btnTilføj, 3, 3);
		this.add(new Label("Pris: "), 3, 1);
		this.add(txfPris, 4, 1);
		this.add(new Label("Antal: "), 3, 2);
		this.add(txfAntal, 4, 2);
		this.add(lvwKøbProdukter, 3, 4, 2, 2);
		this.add(btnFjern, 3, 6);
		this.add(drpList, 1, 0);
		this.add(btnkøb, 3, 10);
		this.add(new Label("Samlet pris: "), 3, 9);
		this.add(txfTotalPris, 4, 9);
		txfTotalPris.setMaxWidth(50);

		lblError.setTextFill(Color.RED);
		this.add(lblError, 4, 10);

		btnFjern.setOnAction(event -> fjernSalgsLinje());
		btnTilføj.setOnAction(event -> opretSalgsLinje());
		btnkøb.setOnAction(event -> Salg());
		drpList.getSelectionModel().selectFirst();
		drpList.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updateProdukter();
		});

		lvwProdukt.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updatePris();
		});

	}

	private void fjernSalgsLinje() {
		lblError.setText("");
		if (lvwKøbProdukter.getSelectionModel().getSelectedItem() != null) {
			lvwKøbProdukter.getItems().remove(lvwKøbProdukter.getSelectionModel().getSelectedItem());
		} else {
			lblError.setText("Vælg en salgslinje");
		}
	}

	private void opretSalgsLinje() {
		double pris = 0;
		if (txfAntal.getText().isEmpty() == false) {
			SalgsLinje produkt = service.opretSalgsLinje(Integer.parseInt(txfAntal.getText()),
					lvwProdukt.getSelectionModel().getSelectedItem());
			lvwKøbProdukter.getItems().addAll(produkt);
			for (int i = 0; i < lvwKøbProdukter.getItems().size(); i++) {
				pris += (lvwKøbProdukter.getItems().get(i).getPris());
			}
			txfTotalPris.setText("" + pris);
			lblError.setText("");
		} else {
			lblError.setText("Angiv antal af produktet");

		}

	}

	private void Salg() {
		if (betaling.getSelectionModel().getSelectedItem() == null) {
			lblError.setText("Vælg betalingstype");
		} else if (lvwKøbProdukter.getItems() == null) {
			lblError.setText("Tilføj et produkt");
		} else {
			ArrayList<SalgsLinje> salgslinje = new ArrayList<>();
			salgslinje.addAll(lvwKøbProdukter.getItems());
			service.opretSalg(LocalDateTime.now(), betaling.getSelectionModel().getSelectedItem(), salgslinje);
			lvwKøbProdukter.getItems().clear();
			txfAntal.clear();
			lvwProdukt.getSelectionModel().clearSelection();
			betaling.getSelectionModel().clearSelection();
			lblError.setText("");
			txfTotalPris.clear();
		}

	}

	public void updateProdukter() {
		lvwProdukt.getItems().clear();
		lvwKøbProdukter.getItems().clear();
		txfAntal.clear();
		txfTotalPris.clear();
		for (int i = 0; i < service.getPrisLinje().size(); i++) {
			if (service.getPrisLinje().get(i).getPrisListe() == drpList.getSelectionModel().getSelectedItem()) {
				lvwProdukt.getItems().add(service.getPrisLinje().get(i));
			}
		}
	}

	public void updatePris() {
		txfPris.clear();
		PrisLinje produkt = lvwProdukt.getSelectionModel().getSelectedItem();

		if (produkt != null) {
			txfPris.setText("" + produkt.getPris());
		}
	}

	public void updateAlt() {
		lvwProdukt.getItems().clear();
		drpList.getItems().clear();
		lvwKøbProdukter.getItems().clear();
		txfAntal.clear();
		txfTotalPris.clear();
		updateProdukter();
		drpList.getItems().addAll(service.getPrisListe());
		drpList.getSelectionModel().selectFirst();
	}

}
