package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.Service;

public class MainApp extends Application {
	private Service service;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() {
		service = Service.getService();
		service.opretObjekter();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus");
		BorderPane pane = new BorderPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.setHeight(500);
		stage.setWidth(800);
		stage.show();
	}

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabOpret = new Tab("Opret");
		Tab tabSalg = new Tab("Salg");
		Tab tabOversigt = new Tab("Salgsoversigt");

		tabPane.getTabs().add(tabOpret);
		tabPane.getTabs().add(tabSalg);
		tabPane.getTabs().add(tabOversigt);

		tabOpret.setContent(new OpretPane());

		SalgsWindow visSalgsWindow = new SalgsWindow();
		tabSalg.setContent(visSalgsWindow);
		tabSalg.setOnSelectionChanged(event -> visSalgsWindow.updateAlt());

		OversigtWindow visOversigtWindow = new OversigtWindow();
		tabOversigt.setContent(visOversigtWindow);
		tabOversigt.setOnSelectionChanged(event -> visOversigtWindow.updateAlt());
	}

}
