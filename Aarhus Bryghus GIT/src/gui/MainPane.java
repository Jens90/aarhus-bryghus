package gui;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import model.Salg;
import model.SalgsLinje;
import service.Service;
import storage.Storage;

public class MainPane extends GridPane {

	private ListView<Produkt> lvwProdukter;
	private ListView<PrisLinje> lvwPrisLinjer;
	private ListView<PrisListe> lvwPrisLister;
	private ListView<Salg> lvwSalg;
	private ListView<SalgsLinje> lvwSalgsLinjer;
	private ChoiceBox<String> cbBetalingsMetoder;

	private Produkt produkt;
	private PrisLinje prisLinje;
	private PrisListe prisListe;

	private boolean gridVisible = false;

	public MainPane(String title) {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(gridVisible);

		GridPane mainPane = new GridPane();

		mainPane.setPadding(new Insets(20));
		mainPane.setHgap(20);
		mainPane.setVgap(10);
		mainPane.setGridLinesVisible(gridVisible);

		this.add(mainPane, 0, 0);

		cbBetalingsMetoder = new ChoiceBox<>();

		cbBetalingsMetoder.getItems().addAll("Dankort", "Kontant", "Mobilepay", "Klippekort", "Regning");
		mainPane.add(cbBetalingsMetoder, 2, 4);
		cbBetalingsMetoder.setValue("Dankort");

		Label lblProdukter = new Label("Alle produkter");
		mainPane.add(lblProdukter, 2, 0);

		Label lblPrisLinjer = new Label("Pris linjer");
		mainPane.add(lblPrisLinjer, 1, 0);

		Label lblPrisLister = new Label("Pris lister");
		mainPane.add(lblPrisLister, 0, 0);

		Label lblValgteProdukter = new Label("Salg");
		mainPane.add(lblValgteProdukter, 0, 4);

		Label lblSolgteProdukter = new Label("Salgslinjer");
		mainPane.add(lblSolgteProdukter, 1, 4);

		Button btnOpret = new Button("Opret");
		mainPane.add(btnOpret, 0, 3);

		btnOpret.setOnAction(event -> opretAction());

		Button btnUpdate = new Button("Opdater");
		mainPane.add(btnUpdate, 1, 3);

		btnUpdate.setOnAction(event -> updateAction());

		Button btnDelete = new Button("Slet");
		mainPane.add(btnDelete, 2, 3);

		btnDelete.setOnAction(event -> deleteAction());

		Button btnRegsalg = new Button("Opret ordrelinje");
		mainPane.add(btnRegsalg, 1, 6);

		btnRegsalg.setOnAction(event -> opretOrdrelinjeAction());

		Button btnTilfoej = new Button("Ny ordre");
		mainPane.add(btnTilfoej, 0, 6);

		// btnTilfoej.setOnAction(event -> tilfoejAction());

		Service.getService().opretObjekter();

		lvwPrisLinjer = new ListView<>();
		mainPane.add(lvwPrisLinjer, 1, 1);
		lvwPrisLinjer.setPrefWidth(250);
		lvwPrisLinjer.setPrefHeight(200);
		// lvwPrisLinjer.getItems().setAll(Service.getService().getPrisLinje());

		lvwPrisLister = new ListView<>();
		mainPane.add(lvwPrisLister, 0, 1);
		lvwPrisLister.setPrefWidth(250);
		lvwPrisLister.setPrefHeight(200);
		lvwPrisLister.getItems().setAll(Service.getService().getPrisListe());

		lvwPrisLister.getSelectionModel().selectedIndexProperty().addListener(observable -> {
			lvwPrisLinjer.getItems().setAll(lvwPrisLister.getSelectionModel().getSelectedItem().getPrisLinjer());
		});

		lvwProdukter = new ListView<>();
		mainPane.add(lvwProdukter, 2, 1);
		lvwProdukter.setPrefWidth(250);
		lvwProdukter.setPrefHeight(200);
		lvwProdukter.getItems().setAll(Service.getService().getProdukt());

		lvwSalg = new ListView<>();
		mainPane.add(lvwSalg, 0, 5);
		lvwSalg.setPrefWidth(200);
		lvwSalg.setPrefHeight(200);
		lvwSalg.getItems().setAll(Service.getService().getSalg());

		lvwSalg.getSelectionModel().selectedIndexProperty().addListener(observable -> {
			ArrayList<SalgsLinje> sl = lvwSalg.getSelectionModel().getSelectedItem().getSalgslinjer();
			lvwSalgsLinjer.getItems().setAll(lvwSalg.getSelectionModel().getSelectedItem().getSalgslinjer());
			if (!sl.isEmpty()) {
				System.out.println(sl.get(0));
			}
		});

		lvwSalgsLinjer = new ListView<>();
		mainPane.add(lvwSalgsLinjer, 1, 5);
		lvwSalgsLinjer.setPrefWidth(200);
		lvwSalgsLinjer.setPrefHeight(200);
		// lvwSalgsLinjer.getItems().setAll(Service.getService().getSalgsLinje());

		// filtrerPrisListe(1);
	}

	public void opretOrdrelinjeAction() {
		Salg salg = lvwSalg.getSelectionModel().getSelectedItem();
		SalgsLinje salgslinje = new SalgsLinje(1, lvwPrisLinjer.getSelectionModel().getSelectedItem());
		salg.addSalgslinje(salgslinje);
		System.out.println(salg.getSalgslinjer().get(0));
	}

	public void opretAction() {
		isSelectedOpret();
	}

	public void updateAction() {
		isSelectedUpdate();
	}

	public void deleteAction() {
		isSelectedDelete();
	}

	public void opretProduktAction() {
		ProduktWindow produktWindow = new ProduktWindow();
		produktWindow.showAndWait();
		updateTotalLists();
	}

	private void updateProduktAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			ProduktWindow produktWindow = new ProduktWindow();
			produktWindow.showAndWait();
			updateTotalLists();
		}
	}

	private void deleteProduktAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		Storage.removeProdukt(produkt);
		updateTotalLists();
	}

	public void opretPrisLinjeAction() {
		PrisLinjeWindow prisLinjeWindow = new PrisLinjeWindow("Opret pris linje");
		prisLinjeWindow.showAndWait();
		updateTotalLists();
	}

	private void updatePrisLinjeAction() {
		PrisLinje prisLinje = lvwPrisLinjer.getSelectionModel().getSelectedItem();
		if (prisLinje != null) {
			PrisLinjeWindow prisLinjeWindow = new PrisLinjeWindow("Opdater pris linje");
			prisLinjeWindow.showAndWait();
			updateTotalLists();
		}
	}

	private void deletePrisLinjeAction() {
		PrisLinje prisLinje = lvwPrisLinjer.getSelectionModel().getSelectedItem();
		Storage.removePrisLinje(prisLinje);
		updateTotalLists();
	}

	public void opretPrisListeAction() {
		PrisListeWindow prisListeWindow = new PrisListeWindow();
		prisListeWindow.showAndWait();
		updateTotalLists();
	}

	private void updatePrisListeAction() {
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		if (prisListe != null) {
			PrisListeWindow prisListeWindow = new PrisListeWindow();
			prisListeWindow.showAndWait();
			updateTotalLists();
		}
	}

	private void deletePrisListeAction() {
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		Storage.removePrisListe(prisListe);
		updateTotalLists();
	}

	private void nyOrdreAction() {

		updateTotalLists();
	}

	// Metoder til at afgøre hvilken opret, update eller delete action skal
	// køres, hvis objektet er valgt i listView

	private void isSelectedOpret() {
		int indexProdukter = lvwProdukter.getSelectionModel().getSelectedIndex();
		int indexPrisLinjer = lvwPrisLinjer.getSelectionModel().getSelectedIndex();
		int indexPrisLister = lvwPrisLister.getSelectionModel().getSelectedIndex();

		if (lvwProdukter.getSelectionModel().isSelected(indexProdukter)) {
			opretProduktAction();
		} else if (lvwPrisLinjer.getSelectionModel().isSelected(indexPrisLinjer)) {
			opretPrisLinjeAction();
		} else if (lvwPrisLister.getSelectionModel().isSelected(indexPrisLister)) {
			opretPrisListeAction();
		}
	}

	private void isSelectedUpdate() {
		int indexProdukter = lvwProdukter.getSelectionModel().getSelectedIndex();
		int indexPrisLinjer = lvwPrisLinjer.getSelectionModel().getSelectedIndex();
		int indexPrisLister = lvwPrisLister.getSelectionModel().getSelectedIndex();

		if (lvwProdukter.getSelectionModel().isSelected(indexProdukter)) {
			updateProduktAction();
		} else if (lvwPrisLinjer.getSelectionModel().isSelected(indexPrisLinjer)) {
			updatePrisLinjeAction();
		} else if (lvwPrisLister.getSelectionModel().isSelected(indexPrisLister)) {
			updatePrisListeAction();
		}
	}

	private void isSelectedDelete() {
		int indexProdukter = lvwProdukter.getSelectionModel().getSelectedIndex();
		int indexPrisLinjer = lvwPrisLinjer.getSelectionModel().getSelectedIndex();
		int indexPrisLister = lvwPrisLister.getSelectionModel().getSelectedIndex();

		if (lvwProdukter.getSelectionModel().isSelected(indexProdukter)) {
			deleteProduktAction();
		} else if (lvwPrisLinjer.getSelectionModel().isSelected(indexPrisLinjer)) {
			deletePrisLinjeAction();
		} else if (lvwPrisLister.getSelectionModel().isSelected(indexPrisLister)) {
			deletePrisListeAction();
		}
	}

	// private void filtrerPrisListe(int index) {
	// ArrayList<PrisLinje> prislinjer = new ArrayList<>();
	// prislinjer =
	// Service.getService().getPrisListe().get(index).getPrisLinjer();
	// lvwProdukter.getItems().clear();
	// for (PrisLinje prisLinje : prislinjer) {
	// lvwProdukter.getItems().add(prisLinje.getProdukt());
	// }
	// lvwPrisLinjer.getItems().setAll(prislinjer);
	// }

	// private void tilføjProdukter() {
	// int index = lvwProdukter.getSelectionModel().getSelectedIndex();
	// if (lvwProdukter.getSelectionModel().isSelected(index)) {
	// lvwProdukter.getItems().setAll(col);
	// }
	// }

	private void updateTotalLists() {
		lvwProdukter.getItems().setAll(Service.getService().getProdukt());
		// lvwPrisLinjer.getItems().setAll(Service.getService().getPrisLinje());
		lvwPrisLister.getItems().setAll(Service.getService().getPrisListe());
		lvwSalg.getItems().setAll(Service.getService().getSalg());
		// lvwSalgsLinjer.getItems().setAll(Service.getService().getSalgsLinje());
	}

	public void updateControls() {
		int selected = 0;
		if (lvwPrisLister.getSelectionModel().getSelectedItem() != null) {
			selected = lvwPrisLister.getSelectionModel().getSelectedIndex();
		}
		lvwPrisLister.getItems().setAll(lvwPrisLinjer.getSelectionModel().getSelectedItem().getPrisListe());
		lvwPrisLister.getSelectionModel().select(selected);
	}
}
