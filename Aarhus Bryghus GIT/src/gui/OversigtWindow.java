package gui;

import java.time.LocalDate;

import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Salg;
import service.Service;

public class OversigtWindow extends GridPane {
	private Service service;
	private GridPane opretPane = new GridPane();
	private ListView<Salg> lvwSalg = new ListView<>();
	private TextField txfBetaling, txfPris, txfAntal, txfTidspunkt, txfKlippekortSolgt, txfKlippekortBrugt,
			txfSolgtIdag;
	private DatePicker kal;

	public OversigtWindow() {
		service = Service.getService();
		this.setHgap(10);
		this.setVgap(10);
		opretPane.setGridLinesVisible(true);
		txfAntal = new TextField();
		txfBetaling = new TextField();
		txfPris = new TextField();
		txfTidspunkt = new TextField();
		txfKlippekortSolgt = new TextField();
		txfKlippekortBrugt = new TextField();
		txfSolgtIdag = new TextField();
		kal = new DatePicker();

		txfAntal.setEditable(false);
		txfBetaling.setEditable(false);
		txfPris.setEditable(false);
		txfTidspunkt.setEditable(false);

		this.add(new Label(""), 5, 1);

		this.add(kal, 1, 1, 2, 1);
		this.add(lvwSalg, 1, 2, 1, 10);
		this.add(new Label("Betalingstype:"), 2, 1);
		this.add(txfBetaling, 3, 1);
		this.add(new Label("Samlet beløb: "), 2, 2);
		this.add(txfPris, 3, 2);
		this.add(new Label("Antal produkter:"), 2, 3);
		this.add(txfAntal, 3, 3);
		this.add(new Label("Tidspunkt :"), 2, 4);
		this.add(txfTidspunkt, 3, 4);

		this.add(new Label("Produkter i dag: "), 2, 9);
		this.add(txfSolgtIdag, 3, 9);
		// this.add(lvwUdleje, 4, 1, 1, 11);

		lvwSalg.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updateTxf();
		});

		kal.setValue(LocalDate.now());
		kal.setOnAction(event -> updateAlt());
	}

	public void updateTxf() {
		Salg salg = lvwSalg.getSelectionModel().getSelectedItem();
		if (lvwSalg.getSelectionModel().getSelectedItem() != null) {
			txfAntal.setText("" + salg.produktAntal());
			txfBetaling.setText("" + salg.getBetaling());
			txfPris.setText("" + salg.prisSalgsLinje());
			txfTidspunkt.setText("" + salg.getTidspunkt());
		}
	}

	public void updateAlt() {
		clear();
		for (int i = 0; i < service.getSalg().size(); i++) {
			if (service.getSalg().get(i).getTidspunkt().getDayOfMonth() == kal.getValue().getDayOfMonth()) {
				lvwSalg.getItems().add(service.getSalg().get(i));
				SolgtIdag();
			}
		}
	}

	private void SolgtIdag() {
		int sum = 0;

		if (lvwSalg.getItems() != null) {
			for (int i = 0; i < lvwSalg.getItems().size(); i++) {
				for (int j = 0; j < lvwSalg.getItems().get(i).getSalgslinjer().size(); j++) {
					sum += lvwSalg.getItems().get(i).produktAntal();
				}
			}
			txfSolgtIdag.setText("" + sum);
		}
	}

	private void clear() {
		lvwSalg.getItems().clear();
		txfAntal.clear();
		txfBetaling.clear();
		txfPris.clear();
		txfTidspunkt.clear();
		txfSolgtIdag.clear();
	}
}
