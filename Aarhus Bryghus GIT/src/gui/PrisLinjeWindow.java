package gui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import service.Service;

public class PrisLinjeWindow extends Stage {
    private PrisLinje prisLinje;
    private TextField txfPris;
    private Produkt produkt = null;
    private PrisListe prisListe = null;

    public PrisLinjeWindow(String title) {
        initStyle(StageStyle.UTILITY);
        initModality(Modality.APPLICATION_MODAL);
        setResizable(false);

        setTitle(title);
        GridPane pane = new GridPane();

        Scene scene = new Scene(pane);
        setScene(scene);

        Label lblPris = new Label("Pris:");
        pane.add(lblPris, 0, 0);
        
        txfPris = new TextField();
        pane.add(txfPris, 1, 0);
        
        Button btnOk = new Button("ok");
        pane.add(btnOk, 0, 1);
        GridPane.setMargin(btnOk, new Insets(10, 10, 0, 10));
        
        btnOk.setOnAction(event -> okAction());

        Button btnAnnuller = new Button("Annuller");
        pane.add(btnAnnuller, 1, 1);
        GridPane.setMargin(btnAnnuller, new Insets(10, 10, 0, 10));
        
        btnAnnuller.setOnAction(event -> annullerAction());
        
    }

    private double okAction() {
        double pris;
        try {
            pris = Integer.parseInt(txfPris.getText());
        }
        catch (NumberFormatException e) {
            pris = 0;
        }
        if ((pris != 0)) {
            prisLinje = Service.getService().opretPrisLinje(pris, produkt, prisListe);
            hide();
        }
        return pris;
    }

    private void annullerAction() {
        hide();
    }
}
