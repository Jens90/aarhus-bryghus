package service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.BetalingsMetoder;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import model.Salg;
import model.SalgsLinje;
import storage.Storage;

public class Service {

	public static Service service;

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public static Service getTestService() {
		return new Service();
	}

	// Getmetoder
	public List<Produkt> getProdukt() {
		return Storage.getProdukt();
	}

	public List<Salg> getSalg() {
		return Storage.getSalg();
	}

	public List<PrisListe> getPrisListe() {
		return Storage.getPrisliste();
	}

	public List<PrisLinje> getPrisLinje() {
		return Storage.getPrislinje();
	}

	public List<SalgsLinje> getSalgsLinje() {
		return Storage.getSalgsLinje();
	}

	// Oprettelsesmetoder

	public Produkt opretProdukt(String navn, String beskrivelse) {
		Produkt produkt = new Produkt(navn, beskrivelse);
		Storage.addProdukt(produkt);
		return produkt;
	}

	public void updateProdukt(String navn, String beskrivelse, Produkt produkt) {
		produkt.setNavn(navn);
		produkt.setBeskrivelse(beskrivelse);

	}

	public Salg opretSalg(LocalDateTime tidspunkt, BetalingsMetoder betaling, ArrayList<SalgsLinje> salgslinjeProdukt) {
		Salg salg = new Salg(tidspunkt, betaling, salgslinjeProdukt);
		Storage.addSalg(salg);
		return salg;
	}

	public PrisListe opretPrisliste(String navn) {
		PrisListe prisliste = new PrisListe(navn);
		Storage.addPrisListe(prisliste);
		return prisliste;
	}

	public PrisLinje opretPrisLinje(double pris, Produkt produkt, PrisListe prisliste) {
		PrisLinje prislinje = new PrisLinje(pris, produkt, prisliste);
		prisliste.addPrisLinje(prislinje);
		Storage.addPrisLinje(prislinje);
		return prislinje;
	}

	public SalgsLinje opretSalgsLinje(int antal, PrisLinje produktPris) {
		SalgsLinje salgslinje = new SalgsLinje(antal, produktPris);
		Storage.addSalgsLinje(salgslinje);
		return salgslinje;
	}

	// Objektoprettelse

	public void opretObjekter() {
		// ---------Flaskeøl
		opretProdukt("Klosterbryg", "33 cl");
		opretProdukt("Sweet Georgia Brown", "33 cl");
		opretProdukt("Extra Pilsner", "33 cl");
		opretProdukt("Celebration", "33 cl");
		opretProdukt("Blondie", "33 cl");
		opretProdukt("Forårsbryg", "33 cl");
		opretProdukt("India Pale Ale", "33 cl");
		opretProdukt("Julebryg", "33 cl");
		opretProdukt("Juletønden", "33 cl");
		opretProdukt("Old Strong Ale", "33 cl");
		opretProdukt("Fregatten Jylland", "33 cl");
		opretProdukt("Imperial Stout", "33 cl");
		opretProdukt("Tribute", "33 cl");
		opretProdukt("Black Monster", "33 cl");

		// ---------Fadøl
		opretProdukt("Klosterbryg", "50 cl");
		opretProdukt("Jazz Classic", "50 cl");
		opretProdukt("Extra Pilsner", "50 cl");
		opretProdukt("Celebration", "50 cl");
		opretProdukt("Blondie", "50 cl");
		opretProdukt("Forårsbryg", "50 cl");
		opretProdukt("India Pale Ale", "50 cl");
		opretProdukt("Julebryg", "50 cl");
		opretProdukt("Imperial Stout", "50 cl");
		opretProdukt("Special", "50 cl");
		opretProdukt("Æblebrus", "50 cl");
		opretProdukt("Chips", "150 g");
		opretProdukt("Peanuts", "150 g");
		opretProdukt("Cola", "50 cl");
		opretProdukt("Nikoline", "50 cl");
		opretProdukt("7up", "50 cl");
		opretProdukt("Vand", "50 cl");

		// -----------Prislister
		opretPrisliste("Butik");
		opretPrisliste("Fredagsbar");

		// ------------PrisLinjer
		// -----Butik
		opretPrisLinje(36, service.getProdukt().get(0), service.getPrisListe().get(0));
		opretPrisLinje(36, service.getProdukt().get(1), service.getPrisListe().get(0));
		opretPrisLinje(36, service.getProdukt().get(2), service.getPrisListe().get(0));
		opretPrisLinje(36, service.getProdukt().get(3), service.getPrisListe().get(0));

		// ----Fredagsbar
		opretPrisLinje(50, service.getProdukt().get(4), service.getPrisListe().get(1));
		opretPrisLinje(50, service.getProdukt().get(5), service.getPrisListe().get(1));
		opretPrisLinje(50, service.getProdukt().get(6), service.getPrisListe().get(1));
		opretPrisLinje(50, service.getProdukt().get(7), service.getPrisListe().get(1));

		// -----------Salgslinje
		opretSalgsLinje(3, service.getPrisLinje().get(0));
		opretSalgsLinje(5, service.getPrisLinje().get(1));
		opretSalgsLinje(5, service.getPrisLinje().get(1));
	}
}
