package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;

import model.BetalingsMetoder;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import model.Salg;
import model.SalgsLinje;

public class SalgslinjeTester {
	PrisListe prisliste = new PrisListe("Fredagsbar");
	Produkt produkt = new Produkt("Fadøl", "50 cl");
	PrisLinje prislinje = new PrisLinje(50, produkt, prisliste);
	LocalDateTime tidspunkt = LocalDateTime.of(2017, 02, 17, 14, 30);
	ArrayList<SalgsLinje> salgsliste = new ArrayList<>();
	Salg salg = new Salg(tidspunkt, BetalingsMetoder.DANKORT, salgsliste);
	SalgsLinje salgslinje = new SalgsLinje(5, prislinje);
	int antalForkert = 7;
	PrisLinje prislinjeForkert = new PrisLinje(36, produkt, prisliste);

	@Test
	public void salgslinje_getAntal_returnAntal() {
		int testResult = salgslinje.getAntal();
		assertEquals(5, testResult, 0.001);
	}

	@Test
	public void salgslinje_getAntal_ReturnError() {
		int testResult = salgslinje.getAntal();
		assertNotSame("Forskelligt antal", antalForkert, testResult);
	}

	@Test
	public void salgslinje_getPrislinje_ReturnPrislinje() {
		PrisLinje testResult = salgslinje.getPrislinje();
		assertEquals(prislinje, testResult);
	}
}
