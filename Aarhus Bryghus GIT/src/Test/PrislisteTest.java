package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import model.PrisLinje;
import model.PrisListe;
import model.Produkt;

public class PrislisteTest {
	PrisListe pl = new PrisListe("Fredagsbar");
	Produkt p = new Produkt("Fadøl", "50 cl");
	ArrayList<PrisLinje> prislinjer = new ArrayList<PrisLinje>();
	PrisLinje prislinje = new PrisLinje(50, p, pl);
	String navnForkert = "Hverdag";

	@Test
	public void prisliste_getNavn_ReturnNavn() {
		String testResult = pl.getNavn();
		assertEquals("Fredagsbar", testResult);
	}

	@Test
	public void prisliste_getNavn_ReturnError() {
		String testResult = pl.getNavn();
		assertNotSame("Forskellige navne", navnForkert, testResult);
	}

	@Test
	public void prisListe_getPrisLinje_ReturnPrisLinje() {
		pl.addPrisLinje(prislinje);
		prislinjer.add(prislinje);
		ArrayList<PrisLinje> testResult = pl.getPrisLinjer();
		Assert.assertEquals(Arrays.asList(prislinje), testResult);
	}

}
