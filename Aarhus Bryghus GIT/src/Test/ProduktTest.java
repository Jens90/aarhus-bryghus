package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

import model.Produkt;

public class ProduktTest {
	Produkt p = new Produkt("Fadøl", "50 cl");
	String navnForkert = "Flaskeøl";
	String beskrivelseForkert = "33 cl";

	@Test
	public void produkt_getNavn_ReturnNavn() {
		String testResult = p.getNavn();
		assertEquals("Fadøl", testResult);
	}

	@Test
	public void produkt_getNavn_ReturnError() {
		String testResult = p.getNavn();
		assertNotSame("Forskellige navne", navnForkert, testResult);
	}

	@Test
	public void produkt_getBeskrivelse_ReturnBeskrivelse() {
		String testResult = p.getBeskrivelse();
		assertEquals("50 cl", testResult);
	}

	@Test
	public void produkt_getBeskrivelse_ReturnError() {
		String testResult = p.getBeskrivelse();
		assertNotSame("Forskellige beskrivelser", beskrivelseForkert, testResult);
	}

}
