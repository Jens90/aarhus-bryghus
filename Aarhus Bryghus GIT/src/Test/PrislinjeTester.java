package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;

import model.PrisLinje;
import model.PrisListe;
import model.Produkt;

public class PrislinjeTester {
	PrisListe prisliste = new PrisListe("Fredagsbar");
	PrisListe prislisteForkert = new PrisListe("Butik");
	Produkt produkt = new Produkt("Fadøl", "50 cl");
	Produkt produktForkert = new Produkt("Flaskeøl", "33 cl");
	PrisLinje prislinje = new PrisLinje(50, produkt, prisliste);
	double prisForkert = 36;

	@Test
	public void prislinje_getPris_ReturnPris() {
		double testResult = prislinje.getPris();
		assertEquals(50, testResult, 0.001);
	}

	@Test
	public void prislinje_getPris_ReturnError() {
		double testResult = prislinje.getPris();
		assertNotSame("Forskellige priser", prisForkert, testResult);
	}

	@Test
	public void prislinje_getProdukt_ReturnProdukt() {
		Produkt testResult = prislinje.getProdukt();
		assertEquals(produkt, testResult);
	}

	@Test
	public void prislinje_getProdukt_ReturnError() {
		Produkt testResult = prislinje.getProdukt();
		assertNotSame("Forskellige produkter", produktForkert, testResult);
	}

	@Test
	public void prislinje_getPrisliste_ReturnPrisliste() {
		PrisListe testResult = prislinje.getPrisListe();
		assertEquals(prisliste, testResult);
	}

	@Test
	public void prislinje_getPrisliste_ReturnError() {
		PrisListe testResult = prislinje.getPrisListe();
		assertNotSame("Forskellige prislister", prislisteForkert, testResult);
	}

}
