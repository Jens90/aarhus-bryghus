package Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;

import model.BetalingsMetoder;
import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import model.Salg;
import model.SalgsLinje;

public class SalgsTester {
	PrisListe prisliste = new PrisListe("Fredagsbar");
	Produkt produkt = new Produkt("Fadøl", "50 cl");
	PrisLinje prislinje = new PrisLinje(50, produkt, prisliste);
	LocalDateTime tidspunkt = LocalDateTime.of(2017, 01, 17, 14, 30);
	ArrayList<SalgsLinje> salgsliste = new ArrayList<>();
	Salg salg = new Salg(tidspunkt, BetalingsMetoder.DANKORT, salgsliste);
	LocalDateTime tidspunktForkert = LocalDateTime.of(2016, 07, 19, 11, 45);
	SalgsLinje salgslinje = new SalgsLinje(5, prislinje);

	@Test
	public void salg_getTidspunkt_ReturnTidspunkt() {
		LocalDateTime testResult = salg.getTidspunkt();
		assertEquals(tidspunkt, testResult);
	}

	@Test
	public void salg_getTidspunkt_ReturnError() {
		LocalDateTime testResult = salg.getTidspunkt();
		assertNotSame("Forskellige tidspunkter", tidspunktForkert, testResult);
	}

	@Test
	public void salg_getBetalingsmetode_ReturnBetalingsMetode() {
		BetalingsMetoder testResult = salg.getBetaling();
		assertEquals(BetalingsMetoder.DANKORT, testResult);
	}

	@Test
	public void salg_getBetalingsmetode_ReturnError() {
		BetalingsMetoder testResult = salg.getBetaling();
		assertNotSame("Forskellige betalingsmetoder", BetalingsMetoder.KONTANT, testResult);
	}

}
