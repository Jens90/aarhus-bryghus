package storage;

import java.util.ArrayList;

import model.PrisLinje;
import model.PrisListe;
import model.Produkt;
import model.Salg;
import model.SalgsLinje;

public class Storage {

	static ArrayList<PrisLinje> prisLinjer = new ArrayList<>();
	private static ArrayList<PrisListe> prisLister = new ArrayList<>();
	private static ArrayList<Produkt> produkter = new ArrayList<>();
	private static ArrayList<Salg> salgsliste = new ArrayList<>();
	private static ArrayList<SalgsLinje> salgsLinjer = new ArrayList<>();

	// Produkt
	public static ArrayList<Produkt> getProdukt() {
		return new ArrayList<>(produkter);
	}

	public static void addProdukt(Produkt produkt) {
		if (!produkter.contains(produkt)) {
			produkter.add(produkt);
		}
	}

	public static void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	// Prisliste
	public static ArrayList<PrisListe> getPrisliste() {
		return new ArrayList<>(prisLister);
	}

	public static void addPrisListe(PrisListe prisliste) {
		// if (!prisLister.contains(prisliste)) {
		prisLister.add(prisliste);
	}
	// }

	public static void removePrisListe(PrisListe prisliste) {
		prisLister.remove(prisliste);
	}

	// Prislinje

	public static ArrayList<PrisLinje> getPrislinje() {
		return new ArrayList<>(prisLinjer);
	}

	public static void addPrisLinje(PrisLinje prislinje) {
		prisLinjer.add(prislinje);
	}

	public static void removePrisLinje(PrisLinje prislinje) {
		prisLister.remove(prislinje);
	}

	// Salg
	public static ArrayList<Salg> getSalg() {
		return new ArrayList<>(salgsliste);
	}

	public static void addSalg(Salg salg) {
		salgsliste.add(salg);
	}

	public static void removeSalg(Salg salg) {
		salgsliste.remove(salg);
	}

	// Salgslinje
	public static ArrayList<SalgsLinje> getSalgsLinje() {
		return new ArrayList<>(salgsLinjer);
	}

	public static void addSalgsLinje(SalgsLinje salgslinje) {
		salgsLinjer.add(salgslinje);
	}

	public static void removeSalgsLinje(SalgsLinje salgslinje) {
		salgsLinjer.remove(salgslinje);
	}

}
