package model;

public enum BetalingsMetoder {
	DANKORT, KONTANT, MOBILEPAY, REGNING
}
