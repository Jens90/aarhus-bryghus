package model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Salg {

	private LocalDateTime tidspunkt;
	private BetalingsMetoder betalingsmetoder;
	private ArrayList<SalgsLinje> salgslinjer = new ArrayList<>();

	public Salg(LocalDateTime tidspunkt, BetalingsMetoder betaling, ArrayList<SalgsLinje> salgslinjeProdukt) {
		this.tidspunkt = tidspunkt;
		betalingsmetoder = betaling;
		salgslinjer = salgslinjeProdukt;

	}

	public void addSalgslinje(SalgsLinje salgslinje) {
		salgslinjer.add(salgslinje);
	}

	public void removeSalgslinje(SalgsLinje salgslinje) {
		salgslinjer.remove(salgslinje);
	}

	public LocalDateTime getTidspunkt() {
		return tidspunkt;
	}

	public void setTidspunkt(LocalDateTime tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

	public BetalingsMetoder getBetaling() {
		return betalingsmetoder;
	}

	public void setBetaling(BetalingsMetoder betaling) {
		this.betalingsmetoder = betalingsmetoder;
	}

	public ArrayList<SalgsLinje> getSalgslinjer() {
		return salgslinjer;
	}

	public int produktAntal() {
		int sum = 0;
		for (int i = 0; i < salgslinjer.size(); i++) {
			sum += salgslinjer.get(i).getAntal();
		}
		return sum;
	}

	public double prisSalgsLinje() {
		double sum = 0;
		for (int i = 0; i < salgslinjer.size(); i++) {
			sum += salgslinjer.get(i).getPris();
		}
		return sum;
	}

	@Override
	public String toString() {
		return "Salg: " + tidspunkt + ", " + betalingsmetoder + ", " + salgslinjer;
	}

}
