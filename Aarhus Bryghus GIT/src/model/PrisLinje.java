package model;

public class PrisLinje {
    
    private double pris;
    private Produkt produkt;
    private PrisListe prisListe;
    
    public PrisLinje(double pris, Produkt produkt, PrisListe prisListe) {
        this.pris = pris;
        this.produkt = produkt;
        this.prisListe = prisListe;
    }

    public double getPris() {
        return pris;
    }

    public void setPris(double pris) {
        this.pris = pris;
    }

    public Produkt getProdukt() {
        return produkt;
    }

    public void setProdukt(Produkt produkt) {
        this.produkt = produkt;
    }

    public PrisListe getPrisListe() {
        return prisListe;
    }

    public void setPrisListe(PrisListe prisListe) {
        this.prisListe = prisListe;
    }
    
    @Override
    public String toString() {
        return produkt + " " + pris + " kr";
    }
    
}
