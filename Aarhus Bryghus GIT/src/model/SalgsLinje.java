package model;

import java.util.ArrayList;

public class SalgsLinje {
	private int antal;
	private Produkt produkt;
	private Salg salg;
	private PrisLinje prislinje;
	public ArrayList<SalgsLinje> produkter = new ArrayList<SalgsLinje>();

	public SalgsLinje(int antal, PrisLinje prislinje) {
		this.antal = antal;
		this.prislinje = prislinje;
	}

	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	public PrisLinje getPrislinje() {
		return prislinje;
	}

	public double getPris() {
		return prislinje.getPris() * antal;
	}

	public ArrayList<SalgsLinje> getProdukter() {
		return produkter;
	}

	@Override
	public String toString() {
		return "SalgsLinje: " + antal + ", " + produkt + ", " + salg + ", " + prislinje;
	}

}
