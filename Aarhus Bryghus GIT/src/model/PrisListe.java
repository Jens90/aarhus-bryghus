package model;

import java.util.ArrayList;

public class PrisListe {

    private String navn;
    private ArrayList<PrisLinje> prisLinjer = new ArrayList<>();

    public PrisListe(String navn) {
        this.navn = navn;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public ArrayList<PrisLinje> getPrisLinjer() {
        return prisLinjer;
    }

    public void setPrisLinjer(ArrayList<PrisLinje> prisLinjer) {
        this.prisLinjer = prisLinjer;
    }

    public void addPrisLinje(PrisLinje prislinje) {
        prisLinjer.add(prislinje);
    }

    @Override
    public String toString() {
        return "PrisListe: " + navn;
    }

}
